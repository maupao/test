---
title: "Home"
order: 0
in_menu: true
---
# Il sito di test

Queste pagine sono da considerarsi come un test per familiarizzare con lo strumento di creazione di mini siti predisposto da [Scribouilli](https://atelier.scribouilli.org/). Attraverso una procedura guidata è possibile creare delle pagine statiche che poi vengono hostate su un repository git, in questo caso GitLab.

Se trovate questo avatar è molto probabile che sia io a scrivere:
![Pinguino]({% link images/zendams-happytux.png %}) 