---
title: "Interessi"
order: 1
in_menu: true
---
I miei interessi spaziano da tutto ciò che garantisce il rispetto e l'uso di software libero intersecato con la didattica e la cittadinanza attiva.

Per questo sono molto curioso e interessato a testare diversi strumenti che possono introdurre la condivisione della conoscenza come approccio fondamentale nella mia esplorazione.

Il mio peregrinare mi ha portato ad incrociare l'associazione [Framasoft](https://framasoft.org) di cui condivido ideali, finalità e strumenti.

Un altro ambiente a me molto caro è [La digitale](https://ladigitale.dev), una raccolta di micro servizi web utili per l'azione didattica. Sono servizi che hanno la caratteristica di poter essere utilizzati senza la necessità di creazione di un account, e quindi senza che ciò che viene fatto possa essere ricondotto ad una persona fisica e magari essere utilizzato a fini di marketing o altro. 