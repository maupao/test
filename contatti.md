---
title: "Contatti"
order: 1
in_menu: true
---
Sono presente in rete in numerosi ambienti. Ecco quelli che frequento di più:

* [Mastodon](https://framapiaf.org/@maupao)
* [Il mio sito personale](https://www.paolomauri.it)
* [Spazio degli appunti](https://writefreely.maupao.nohost.me) 